// ==UserScript==
// @name           FBase Lib
// @description    Base library
// @version        0.0.2
// ==/UserScript==

const FOUR_MINUTES = 4 * 60 * 1000;
const wait = ms => new Promise(resolve => setTimeout(resolve, ms || 3000));

Element.prototype.isVisible = function() {
    return !!(this.offsetWidth||this.offsetHeight||this.getClientRects().length);
};
Element.prototype.isUserFriendly = function(selector) {
    let e = selector ? this.querySelector(selector) : this;
    return e && e.isVisible()  ? e : null;
};
Document.prototype.isUserFriendly = Element.prototype.isUserFriendly;

class CrawlerWidget {
    constructor(params) {
        if (!params || !params.selector) {
            throw new Error('CrawlerWidget requires a selector parameter');
        }
        this.context = this.context || document;
        Object.assign(this, params);
    }

    get isUserFriendly() {
        this.element = this.context.isUserFriendly(this.selector);
        return this.element;
    }
}

class CaptchaWidget extends CrawlerWidget {
    constructor(params) {
        super(params);
    }

    solve() { return true; }

    async isSolved() { return false; }
}

class HCaptchaWidget extends CaptchaWidget {
    constructor(params) {
        let defaultParams = {
            selector: '.h-captcha > iframe',
            waitMs: [1000, 5000],
            timeoutMs: FOUR_MINUTES
        };
        for (let p in params) {
            defaultParams[p] = params[p];
        }
        super(defaultParams);
    }

    async isSolved() {
        return wait().then( () => {
            if (this.isUserFriendly && this.element.hasAttribute('data-hcaptcha-response') && this.element.getAttribute('data-hcaptcha-response').length > 0) {
                return Promise.resolve(true);
            }
            return this.isSolved();
        });
    }
}

class Storage {
    constructor(config = {}) {
        this.prefix = config && config.prefix ? config.prefix : 'fbase_';
        this.config = config;
    }

    write(key, value, parseIt = false) {
        GM_setValue(this.prefix + key, parseIt ? JSON.stringify(value) : value);
    }

    read(key, parseIt = false) {
        let value = GM_getValue(this.prefix + key);
        if(value && parseIt) {
            value = JSON.parse(value);
        }
        return value;
    }

    pushToArray(key, value) {
        let arr = this.read(key, true) || [];
        if(Array.isArray(value)) {
            arr = [...arr, ...value];
        } else {
            arr = [...arr, value];
        }
        this.write(key, arr, true);
    }
}

class SettingsPopup {
    constructor(fields = [], storage) {
        this.fields = {
            username: fields.includes('username'),
            email: fields.includes('email'),
            faucetpay_email: fields.includes('fp-email'),
            faucetpay_btc: fields.includes('fp-btc'),
            password: fields.includes('password')
        };
        this.storage = storage;
        this.loadData();
        GM_registerMenuCommand('Settings...', function () { this.show(); }.bind(this));
    }

    css = {
        'fbase-popup':
        `.fbase-box-wrapper {
position: fixed;
top: 50%;
left: 50%;
transform: translate(-50%, -50%);
backgroundColor: white;
border: 1px solid black;
padding: 10px;
}

.fbase-box{
grid-area: box;
position: absolute;
top:  50%;
left: 50%;
transform: translate(-50%, -50%);
width: 400px;
background-color: #FFF;
padding: 40px;
box-sizing: border-box;
border: 1px solid rgba(0,0,0,.1);
/* box-shadow: 0 30px 20px rgba(0,0,0,.2); */
box-shadow: 0 3px 60px rgba(57,73,76,0.35);

}

.fbase-box h2{
margin: 0 0 40px;
padding: 0;
color: #222;
text-transform: uppercase;
text-align: center;
}

.fbase-box input, .fbase-box button{
padding: 10px 0;
margin-bottom: 30px;
}

.fbase-box textarea{
height: 80px;
padding: 10px 0;
margin-bottom: 40px;
}

.fbase-box input, .box textarea, .fbase-box button {
width: 100%;
box-sizing: border-box;
box-shadow: none;
outline: none;
border: none;
border-bottom: 2px solid #999;
}

.fbase-box button{
font-size: 1.1em;
border-bottom: none;
cursor: pointer;
background: #37a000;
color: #FFF;
margin-bottom: 0;
text-transform: uppercase;
}

.fbase-box button + button {
margin-left: 10px;
}

.fbase-box form div{
position: relative;
}

.fbase-box form div label {
position: absolute;
top: 10px;
left: 0;
color: #999;
pointer-events: none;
text-transform: uppercase;
transition: .5s;
}

.fbase-box input:focus ~ label, .box textarea:focus ~ label,
.fbase-box input:valid ~ label, .box textarea:valid ~ label {
top: -12px;
left: 0;
color: #37a000;
font-size: 1em;
font-weight: bold;
}

.fbase-box input:focus ~ label, .box textarea:focus ~ label,
.fbase-box input:valid, .box textarea:valid {
border-bottom: 2px solid #37a000;
}
`
    }

    addCss() {
        if (document.querySelector('#fbase-css')) {
            console.log('css already added');
            return;
        }
        console.log('adding css');
        let cssElm = document.createElement('style');
        cssElm.setAttribute('type', 'text/css');
        cssElm.setAttribute('id', 'fbase-css');
        cssElm.innerHTML = this.css['fbase-popup'];
        document.head.appendChild(cssElm);
        this.cssAdded = true;
    }

    render() {
        this.popup = document.querySelector('fbase-box-wrapper');
        if (!this.popup) {
            console.log('creating popup element');
            this.popup = document.createElement('div');
            this.popup.classList.add('fbase-box-wrapper');
            document.body.appendChild(this.popup);
        }

        let html = `<div class="fbase-box"><h2>Settings</h2><form id="someForm" action="">`;
        // popup.innerHTML += `<textarea required placeholder=" "></textarea><label>Message</label></div>`;
        this.data.forEach( field => {
            switch (field.key) {
                case 'email':
                    html += `<div><input type="email" required placeholder=" " id="${this.storage.prefix}${field.key}" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="${field.value || ''}" />`;
                    break;
                case 'password':
                    html += `<div><input type="password" id="${this.storage.prefix}${field.key}" required placeholder=" " value="${field.value || ''}">`;
                    break;
                case 'username':
                default:
                    html += `<div><input type="text" id="${this.storage.prefix}${field.key}" required placeholder=" " value="${field.value || ''}">`;
                    break;
            }
            html += `<label>${field.key.replace('_', ' ')}</label></div>`;
        });
        // for (const key in this.fields) {
        //     // console.log(key);
        //     // console.log(this.fields[key]);
        //     if (this.fields[key]) {
        //         switch (key) {
        //             case 'email':
        //                 html += `<div><input type="email" name="" required placeholder=" " id="${this.storage.prefix}${key}" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"/>`;
        //                 break;
        //             case 'password':
        //                 html += `<div><input type="password" name="" id="${this.storage.prefix}${key}" required placeholder=" ">`;
        //                 break;
        //             case 'username':
        //             default:
        //                 html += `<div><input type="text" name="" id="${this.storage.prefix}${key}" required placeholder=" ">`;
        //                 break;
        //         }
        //         html += `<label>${key.replace('_', ' ')}</label></div>`;
        //     }
        // }
        html += `<div style="display: flex;"><button type="button" data-fn="cancel">Cancel</button><button type="button" data-fn="save">Save</button></div></form></div>`;
        console.log(html);
        this.popup.innerHTML = html;
    }

    addButtonListeners() {
        if (!this.popup) {
            return;
        }
        this.popup.querySelector('button[data-fn="save"]').addEventListener('click', this.saveChanges.bind(this), true);
        this.popup.querySelector('button[data-fn="cancel"]').addEventListener('click', this.close.bind(this), true);
        console.log('adding listeners');

        // displayed = true;
        // update();
    }

    show() {
        if(this.isOpened) {
            console.log('not showing - already opened');
            return;
        }
        this.addCss();
        this.render();
        this.addButtonListeners();
    }

    loadData() {
        let values = this.storage.read(`prompt_settings`, true);
        if (!values) {
            values = [];
        }
        let needsSave = false;
        for(const field in this.fields) {
            if (this.fields[field]) {
                if (values.findIndex(x => x.key == field) == -1) {
                    needsSave = true;
                    values.push({
                        key: field,
                        value: null
                    });
                }
            } else {
                // TODO: remove fields that became obsolete
            }
        }
        if (needsSave) {
            this.storage.write(`prompt_settings`, values, true);
        }
        this.data = values;
    }

    get(field) {
        if(!this.data) {
            return null;
        }
        let fieldIdx = this.data.findIndex(x => x.key == field);
        return fieldIdx > -1 ? this.data[fieldIdx].value : null;
    }

    saveChanges(event) {
        event.preventDefault();
        let form = document.querySelector('#someForm');
        if (!form.checkValidity()) {
            console.log('invalid form');
            form.reportValidity();
            return;
        }

        let newData = [];
        this.data.forEach( field => {
            let elm = document.querySelector(`#${this.storage.prefix}${field.key}`);
            newData.push({
                key: field.key,
                value: elm.value
            });
        });
        this.data = newData;
        this.storage.write(`prompt_settings`, this.data, true);

        console.log('save changes clicked');
        this.close();
    }

    close(event) {
        if (event && typeof event.preventDefault === 'function') {
            event.preventDefault();
        }
        this.popup.parentElement.removeChild(this.popup);
        this.popup = undefined;
    }
}

class BaseSettings {
    constructor(storage) {
        this.storage = storage;
        this.data = this.storage.read(`prompt_settings`, true);
    }

    get(field) {
        if(!this.data) {
            return null;
        }
        let fieldIdx = this.data.findIndex(x => x.key == field);
        return fieldIdx > -1 ? this.data[fieldIdx].value : null;
    }
}

class FBase {
    constructor(settings = {}) {
        this.settings = {
            storagePrefix: 'fbase_',
            startAt: null,
            fields: [],
            defaults: {
                useSleep: false, // NOT_IMPLEMENTED - if true it will require a timeframe
                maxRuntime: 240, // in seconds. As a timeout / when to close the tab
                nextRunAfter: 6 * 60 + 5, // in minutes
                onErrorWait: 15, // in minutes - NOT_IMPLEMENTED
                runAlone: false // when false, the script will only be executed on the 'target' site if it's opened from CL
            },
            schedule: {},
        };
        Object.assign(this.settings, settings);
        this.getGrants();
        if (this.grants.hasStorage) {
            this.storage = new Storage({ prefix: this.settings.storagePrefix });
        } else {
            // TODO: maybe error or warning based on required settings
        }
        if (this.settings.fields.length > 0 && this.grants.hasPopup) {
            if (location.host == 'criptologico.com') {
                // Loading settings with popup
                this.popup = new SettingsPopup(this.settings.fields, this.storage);
            } else {
                // Loading basic settings reader
                this.popup = new BaseSettings(this.storage);
            }
        } else {
            // TODO: maybe error or warning based on required settings
        }
        this.loadSchedule();
        this.start();
        // this.createRollObject();
    }

    isHook() {
        return location.host == 'criptologico.com';
    }

    start() {
        if (this.isHook()) {
            if (this.current && this.current.active) {
                // TODO: trigger timeout logic
                return;
            }
            if (!this.settings.schedule.nextRun) {
                this.settings.schedule.nextRun = new Date().getTime();
                console.log('Opening NOW');
                this.storage.write('current', { active: true }, true);
                GM_openInTab(this.settings.startAt, { active: false });
                return;
            } else {
                setTimeout(() => {
                    console.log('Opening in a while');
                    this.storage.write('current', { active: true }, true);
                    GM_openInTab(this.settings.startAt, { active: false });
                }, this.settings.schedule.nextRun - (new Date().getTime()));
                return;
            }
        }
    }

    loadSchedule() {
        let storedSchedule = this.storage.read(`schedule`, true);
        // TODO: validations on defaults
        this.settings.schedule = storedSchedule ? storedSchedule : this.settings.defaults;
        this.current = this.storage.read('current', true);
        return;
    }

    breakExecution() {
        console.log('@breakExec');
        console.log(location.host == 'criptologico.com');
        console.log(this.settings.schedule.runAlone);
        console.log(!this.current && this.current.active);
        if (location.host == 'criptologico.com') return true; // stop if hook url
        if (this.settings.schedule.runAlone) return false; // continue if can run alone
        // otherwise, check if it was triggered by the schedule
        return !(this.current && this.current.active);
    }

    success(data = {}) {
        console.log('@success');
        if (!this.current.data) {
            this.current.data = {}
        }
        if (data != {}) {
            Object.assign(this.current.data, data);
        }
        this.current.active = false;
        this.current.success = true;
        this.storage.write('current', this.current, true);
        // TODO: from Hook, use a valueListener to monitor current
        setTimeout(window.close, 5000);
    }

    getGrants() {
        this.grants = {
            canWrite: false,
            canRead: false,
            canMenuCommand: false,
            hasPopup: false,
            hasStorage: false
        };
        let grants = GM_info.script.grant;
        if (!grants || grants.includes('none')) {
            return;
        }
        this.grants.canWrite = grants.includes('GM_setValue');
        this.grants.canRead = grants.includes('GM_getValue');
        this.grants.canMenuCommand = grants.includes('GM_registerMenuCommand');
        this.grants.hasStorage = this.grants.canWrite && this.grants.canRead;
        this.grants.hasPopup = this.grants.hasStorage && this.grants.canMenuCommand;
        if (!this.grants.hasStorage) {
            throw new Error('Invalid FBase implementation');
        }
    }

    get(field) {
        if (!field || !this.grants.hasPopup) return null;
        // TODO: if no grant GM_getValue => return null
        return this.popup.get(field);
    }

    // createRollObject() {
    //     this.roll = {
    //         current: null,
    //         getCurrent: function() {
    //             if (this.current) {
    //                 return this.current;
    //             }
    //             this.current = this.storage.read('lastRun', true);
    //             if (!this.current) {
    //                 return this.start();
    //             }
    //             return this.current;
    //         }.bind({ storage: this.storage }),

    //         setCurrent: function(newCurrent) {
    //             this.current = newCurrent;
    //             this.storage.write('lastRun', this.current, true);
    //             return this.current;
    //         }.bind({ storage: this.storage }),

    //         start: function() {
    //             this.current = {
    //                 start: new Date().getTime(),
    //                 end: null
    //             };
    //             return this.setCurrent(this.current);
    //         },
    //         update: function(key, value) {
    //             this.getCurrent()[key] = value;
    //             return this.setCurrent(this.current);
    //         },
    //     };
    // }

    createHash(str) {
        var hash = 0, i, chr;
        if (str.length === 0) return hash;
        for (i = 0; i < str.length; i++) {
            chr = str.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0;
        }
        return hash;
    }

    getScriptData() {
        this.scriptData = this.storage.read('script_data', true);
        if (this.scriptData) {
            // Only check for version changes
            if (GM_info.script.version != this.scriptData.version) {
                console.log('New version detected'); // TODO: do something if required
                this.scriptData.version = GM_info.script.version;
                this.storage.write('script_data', this.scriptData, true);
            }
            return this.scriptData;
        }

        // First time reading script data:
        let info;
        this.scriptData = {};

        try {
            info = GM_info;
        } catch (err) { console.log('Unable to retrieve script info'); return false; }
        if (!info.script) {
            console.log('No script data found...'); return false;
        }
        if (!info.script.name) {
            console.log('When the script has no name...'); return false;
        }

        this.scriptData.name = info.script.name;
        this.scriptData.version = info.script.version;
        if (info.script.author && info.script.author.toLowerCase() != 'you') {
            this.scriptData.author = info.script.author;
        } else {
            this.scriptData.author = 'unknown';
        }

        this.scriptData.stringId = `[${this.scriptData.author}] ${info.script.name}`;
        this.scriptData.hashId = `us-${this.createHash(this.scriptData.stringId).toString(20)}`;
        this.storage.write('script_data', this.scriptData, true);

        return this.scriptData;
    }
}

// ==UserScript==
// @name           FBase Lib
// @description    Base library
// @version        0.0.3
// ==/UserScript==

const FOUR_MINUTES = 4 * 60 * 1000;
const wait = ms => new Promise(resolve => setTimeout(resolve, ms || 3000));

Element.prototype.isVisible = function() {
    return !!(this.offsetWidth||this.offsetHeight||this.getClientRects().length);
};

Element.prototype.isUserFriendly = function(selector) {
    let e = selector ? this.querySelector(selector) : this;
    return e && e.isVisible()  ? e : null;
};

Document.prototype.isUserFriendly = Element.prototype.isUserFriendly;

class CrawlerWidget {
    constructor(params) {
        if (!params || !params.selector) {
            throw new Error('CrawlerWidget requires a selector parameter');
        }
        this.context = this.context || document;
        Object.assign(this, params);
    }

    get isUserFriendly() {
        this.element = this.context.isUserFriendly(this.selector);
        return this.element;
    }
}

class CaptchaWidget extends CrawlerWidget {
    constructor(params) {
        super(params);
    }

    solve() { return true; }

    async isSolved() { return false; }
}

class HCaptchaWidget extends CaptchaWidget {
    constructor(params) {
        let defaultParams = {
            selector: '.h-captcha > iframe',
            waitMs: [1000, 5000],
            timeoutMs: FOUR_MINUTES
        };
        for (let p in params) {
            defaultParams[p] = params[p];
        }
        super(defaultParams);
    }

    async isSolved() {
        return wait().then( () => {
            if (this.isUserFriendly && this.element.hasAttribute('data-hcaptcha-response') && this.element.getAttribute('data-hcaptcha-response').length > 0) {
                return Promise.resolve(true);
            }
            return this.isSolved();
        });
    }
}

class Storage {
    constructor(config = {}) {
        this.prefix = config && config.prefix ? config.prefix : 'fbase_';
        this.config = config;
    }

    write(key, value, parseIt = false) {
        GM_setValue(this.prefix + key, parseIt ? JSON.stringify(value) : value);
    }

    read(key, parseIt = false) {
        let value = GM_getValue(this.prefix + key);
        if(value && parseIt) {
            value = JSON.parse(value);
        }
        return value;
    }

    pushToArray(key, value) {
        let arr = this.read(key, true) || [];
        if(Array.isArray(value)) {
            arr = [...arr, ...value];
        } else {
            arr = [...arr, value];
        }
        this.write(key, arr, true);
    }
}    

class BaseSettings {
    constructor(storage) {
        this.storage = storage;
        this.data = this.storage.read(`customs`, true);
    }

    get(field) {
        if(!this.data) {
            return null;
        }
        let fieldIdx = this.data.findIndex(x => x.key == field);
        return fieldIdx > -1 ? this.data[fieldIdx].value : null;
    }
}

class FBase {
    constructor(settings = {}) {
        this.settings = {
            storagePrefix: 'fbase_',
            startAt: null,
            fields: [],
            defaults: {
                useSleep: false, // NOT_IMPLEMENTED - if true it will require a timeframe
                maxRuntime: 240, // in seconds. As a timeout / when to close the tab
                nextRunAfter: 6 * 60 + 5, // in minutes
                onErrorWait: 15, // in minutes - NOT_IMPLEMENTED
                runAlone: false // when false, the script will only be executed on the 'target' site if it's opened from CL
            },
            schedule: {},
        };
        Object.assign(this.settings, settings);
        this.validateGrants();
        this.storage = new Storage({ prefix: this.settings.storagePrefix });

        if (this.settings.fields.length > 0) {
            if (location.host != 'criptologico.com') {
                // Loading basic settings reader
                this.popup = new BaseSettings(this.storage);
            }
        } else {
            // TODO: maybe error or warning based on required settings
        }

        this.start();
        // this.createRollObject();
    }

    isHook() {
        return location.host == 'criptologico.com';
    }

    start() {
        if (this.isHook()) {
            this.atHook();
        } else {
            this.atTarget();
        }
    }

    atHook() {
        this.loadScriptData();
        if (this.registerForDiscovery()) {
            // TODO: LS listener to check if it was discovered...
        }
        return;

        // create listeners for localStorage and gm_storage
        if (this.current && this.current.active) {
            // TODO: trigger timeout logic
            return;
        }
        if (!this.settings.schedule.nextRun) {
            this.settings.schedule.nextRun = new Date().getTime();
            console.log('Opening NOW');
            this.storage.write('current', { active: true }, true);
            GM_openInTab(this.settings.startAt, { active: false });
            return;
        } else {
            setTimeout(() => {
                console.log('Opening in a while');
                this.storage.write('current', { active: true }, true);
                GM_openInTab(this.settings.startAt, { active: false });
            }, this.settings.schedule.nextRun - (new Date().getTime()));
            return;
        }
    }

    atTarget() {
        this.loadSchedule();
        this.breakOrContinue();
    }

    loadSchedule() {
        let storedSchedule = this.storage.read(`schedule`, true);
        // TODO: validations on defaults
        this.settings.schedule = storedSchedule ? storedSchedule : this.settings.defaults;
        this.current = this.storage.read('current', true);
        return;
    }

    breakOrContinue() {
        console.log('@breakOrContinue');
        // TODO: define conditions that should make the script fail to stop execution
        if('breakme' == 'breakme') {
            throw Error('FBase prevented the execution of the script');
            return;
        }
        console.log(this.settings.schedule.runAlone);
        console.log(!this.current && this.current?.active);
        if (this.settings.schedule.runAlone) return false; // continue if can run alone
        // otherwise, check if it was triggered by the schedule
        return !(this.current && this.current.active);
    }

    success(data = {}) {
        console.log('@success');
        if (!this.current.data) {
            this.current.data = {}
        }
        if (data != {}) {
            Object.assign(this.current.data, data);
        }
        this.current.active = false;
        this.current.success = true;
        this.storage.write('current', this.current, true);
        // TODO: from Hook, use a valueListener to monitor current
        setTimeout(window.close, 5000);
    }

    validateGrants() {
        this.grants = {
            canWrite: false,
            canRead: false,
            hasStorage: false
        };
        let grants = GM_info.script.grant;
        if (!grants) {
            return;
        }
        if (grants.includes('none')) {
            throw new Error('Invalid FBase implementation. Please remove //@grant none from your script.');
            return;
        }
        this.grants.canWrite = grants.includes('GM_setValue');
        this.grants.canRead = grants.includes('GM_getValue');
        // this.grants.canMenuCommand = grants.includes('GM_registerMenuCommand');
        this.grants.hasStorage = this.grants.canWrite && this.grants.canRead;
        // this.grants.hasPopup = this.grants.hasStorage && this.grants.canMenuCommand;
        if (!this.grants.hasStorage) {
            throw new Error('Invalid FBase implementation. You might be missing a @grant for GM_setValue or GM_getValue');
        }
        if (!GM_info.script.matches.includes('https://criptologico.com/tools/cc')) {
            // TODO: maybe implement an alert for this stuff...
            throw new Error('Invalid FBase implementation. Make sure your script has this line: // @match https://criptologico.com/tools/cc');
        }
    }

    get(field) {
        if (!field) return null;
        return this.popup.get(field) || '';
    }

    createHash(str) {
        var hash = 0, i, chr;
        if (str.length === 0) return hash;
        for (i = 0; i < str.length; i++) {
            chr = str.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0;
        }
        return hash;
    }

    loadScriptData() {
        this.scriptData = this.storage.read('script_data', true);
        if (!this.scriptData) {
            this.scriptData = {};
            this.scriptData.name = GM_info.script.name;
            this.scriptData.version = GM_info.script.version;
            if (GM_info.script.author && GM_info.script.author.toLowerCase() != 'you') {
                this.scriptData.author = GM_info.script.author;
            } else {
                this.scriptData.author = 'unknown';
            }
            this.scriptData.stringId = `[${this.scriptData.author}] ${this.scriptData.name}`;
            this.scriptData.hashId = `fbase-us-${this.createHash(this.scriptData.stringId).toString(20)}`;
            this.storage.write('script_data', this.scriptData, true);
        } else {
            let currentVersion = GM_info.script.version;
            if (currentVersion != this.scriptData.version) {
                console.warn('Version changed! TODO: validate if new fields are required and register again');
            } else {
                console.log('Same version...');
            }
        }
        console.log(this.scriptData.stringId, this.scriptData.hashId, this.scriptData);
    }

    registerForDiscovery() {
        let lsEntry = localStorage.getItem(this.scriptData.hashId);
        if (lsEntry) {
            console.log('Already registered for discovery');
            return false;
        } else {
            console.log('Registering for discovery');
            localStorage.setItem(this.scriptData.hashId, JSON.stringify(this.scriptData));
            return true;
        }
    }
}